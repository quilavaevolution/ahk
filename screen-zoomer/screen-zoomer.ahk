#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force
CoordMode, Mouse, Screen  

Menu, Tray, Icon, zoom.ico

^!F12::
    MouseGetPos, xpos, ypos

    w := (A_ScreenWidth / 2)

    if (xpos > w)
    {
        x := A_ScreenWidth / 4
        MouseMove, %x%, %ypos%
    }
    else 
    {
        x := (3 * A_ScreenWidth) / 4
        MouseMove, %x%, %ypos%
    }

    ; newxpos := A_ScreenWidth - xpos 
    ; MouseMove, %newxpos%, %ypos%, 00 