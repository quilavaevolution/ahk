#!/bin/bash

# # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# INTERNET LIVE STREAM RECORDING SCRIPT
# record.sh -d DURATION_TO_ATTEMPT_RECORDING -i INPUT_URL
# # # # # # # # # # # # # # # # # # # # # # # # # # # # #


# Set working directory to script location
cd "$(dirname "$0")"

record_url=""
script_duration=21600

while getopts ":d:i:" opt; do
	case $opt in
		d) script_duration="$OPTARG"
		;;	
		i) record_url="$OPTARG"
		;;
		\?) echo "Invalid option -$OPTARG"
		;;
	esac
done

script_end_time=$(( $(date +%s) + script_duration))

echo ""
echo "### Welcome to Automated Livestream Recording by B0sh ###"
echo "record_url = ${record_url}"
echo "script_duration = ${script_duration}"

while :
do
	output_url=`date`
	echo ""
	echo ""
	echo "### Recording from ${record_url} at \"${output_url}.mp3\" ###"
	echo ""

	ffmpeg -hide_banner -i ${record_url} -icy 1 -c copy "${output_url}.mp3"

	# Did you successfully record a file
	if [[ -f "${output_url}.mp3" ]]
	then
		icy_name=`ffprobe -loglevel error -show_entries format_tags=icy-name -of default=noprint_wrappers=1:nokey=1 "${output_url}.mp3"`
		stream_title=`ffprobe -loglevel error -show_entries format_tags=StreamTitle -of default=noprint_wrappers=1:nokey=1 "${output_url}.mp3"`

		new_file=`date +%F\ %H\-%M`

		echo ""
		echo "### Redirecting file to \"${icy_name} ${new_file}.mp3\" ###"
		echo "### Icy Name: ${icy_name} ###"
		echo "### Stream Title: ${stream_title} ###"
		echo ""

		
		# Is the MP3 name Zero length, if so we avoid the front space in filename
		if [[ -z "${icy_name}" ]]
		then
			mv "${output_url}.mp3" "${new_file}.mp3"
		else
			mv "${output_url}.mp3" "${icy_name} ${new_file}.mp3"
		fi
	fi

	# break out of the loop if duration has passed
	if [[ "${script_end_time}" -lt `date +%s` ]]
	then	
		echo "Leaving script because time is up"
		break
	fi

	sleep 69 
done
