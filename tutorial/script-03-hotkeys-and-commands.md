# Hotkeys and Commands

The two main components to any AutoHotkey script are Hotkeys and Commands. Let's begin with Hotkeys.

## Hotkeys

If you haven't heard the term Hotkey before, a quick google search reveals... Ah AutoHotkey, good old recurive defetion. Scrolling down a bit more we see "keyboard shortcut".

> F1::
>   Send, Hello World!
>   Return

If we look at our previous script from the last video, the `F1 colon colon` syntax tells AutoHotkey that we want to trigger the following lines of code with the `F1` key. In fact this works with every key on the keyboard - and keys on other people's keyboard like `F24`. [show image of a crazy keyboard]

Having a hotkey for just letters or numbers would quickly be annoying though, since using your keyboard normally would becomes difficult. That's why AutoHotkey has Modifer Symbols. 

> Make picture of keyboard and then arrows saying which modifer is which.

* ^ Caret corresponds to control
* ! Excalatmion mark corresponds to alt
* + Plus corresponds to shift
* # and number sign is the windows key

You can mix and match these as much as you want to create combinations of modifer keys. For example Control Shift C is `^+c::`. 

If you want to test yourself, pause and see if you can create hotkeys for these combinations

> Leave up the refrence with the keys. just testing for conceptuality and the :: remebering
> Control X
> Win F5
> Control Alt R

Here's the answers:



One of my favorite pages is the AutoHotkey list of keys. It can be hard to remember which modifier is which so this a great refrence to have open when writing scripts. 

> Scrolling through page
> https://www.autohotkey.com/docs/KeyList.htm

# Commands

Now that we have a better understanding of what hotkeys are let's talk about the commands that get executed.

> F1::
>   Send, Hello World!
>   Return

Continuing to look at our example script, both `Send` and `Return` are commands. Think of commands as actions or steps that a computer runs. Here the `Send` command types out the text `Hello World!`. The `Return` command is signaling the end of the hotkey. 

Many times we want to display a message back to use with some kind of output. One way to do that is through the `MsgBox` command. It works exactly like send, only it pops up a box with the message contents. Let's change our program to use `MsgBox`.

> F1::
>   MsgBox, Hello World!
>   Return

Save, and reload the script. Now, when pressing F1 we can see a nice dialog box telling us our message. Great! 

You may have noticed the commas that we are leaving after the command name. These seperate different paramaters from each other. Paramaters are like different inputs to a command. So far our commands have only used one parameter. Let's take a look at the `TrayTip` command. This command sends a windows notification to your computer. The AutoHotkey documentation shows us that the first paramater is a Title, and the second paramater is Text. 

> F1::
>   TrayTip, AHK Tutorial, Hello World!
>   Return

For our title I'll make it AHK Tutorial. Then we need another comma and our message content, Hello World. Save and reload the script. Now, when pressing F1 we can see a windows notification.

This was a short introduction into how to use Hotkeys and Commands in AutoHotkey. There's hundreds of commands in AutoHotkey, we won't be able to go over all of them in this series, so I would encourage you to look around the list of commands and just see the kinds of things that AutoHotkey can do.

> https://www.autohotkey.com/docs/commands/index.htm